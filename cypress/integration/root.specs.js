describe('Root endpoint', () => {
  it('Returns information about the current API', () => {
      cy.request('/')
        .then((response) => {
          expect(response.status).to.equal(200)
          expect(response.body).to.have.property('name', "Recommerce Pricing API")
          expect(response.body).to.have.property('version', "2.0.0")
        })
  })
})
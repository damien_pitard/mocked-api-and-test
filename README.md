Pricing API
===========

* standalone OpenAPI spec in openapi.yml
* API mocked in mock.json, thanks to (json-server)[https://github.com/typicode/json-server]
* tests in cypress/integration

## Testing

Install dependencies:

    npm i

Run all tests:

    npm test